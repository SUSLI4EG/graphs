#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <set>
#include <stack>
#include <queue>
using namespace std;
int sign(int i) {
	if (i == 0) { return 0; }
	else { return 1; };
}

class DSU
{
private:
	vector<int> p;
	vector<int> rank;
public:
	void makeSet(int x)
	{
		p[x] = x;
		rank[x] = 0;
	}
	DSU(int N)
	{
		p.resize(N + 1);
		rank.resize(N + 1);
		for (int i = 0; i <= N; ++i) {
			makeSet(i);
		}
	}
	int find(int x)
	{
		return (x == p[x] ? x : p[x] = find(p[x]));
	}
	void unite(int x, int y) {
		if ((x = find(x)) == (y = find(y)))
			return;

		if (rank[x] <  rank[y])
			p[x] = y;
		else
			p[y] = x;

		if (rank[x] == rank[y])
			++rank[x];
	}	
	bool isSingleTree()
	{
		for (int i = 1; i < p.size() - 1; i++)
		{
			if (find(p[i]) != find(p[i + 1]))
			{
				return false;
			}
		}
		return true;
	}
};
class Graph {
private:
	vector<vector<int>> AdjMatrix; //C
	vector<vector<pair<int, int>>> AdjList; //L
	vector<vector<int>> ListOfEdges; //1-�� � 2-�� �������� ������ ������ - ������� �����, 3-�� - ��� (���� ����) E
	unsigned int vertexs;
	unsigned int ribs;
	char type_represent;
	bool isWeighed;
	bool isOriented;
	//������� �����������������, ����� ������������
	void ListSort(Graph &graph)
	{
		graph.transformToListOfEdges();
		for (int i = 0; i < ribs; i++) {
			int buff_f, buff_t, buff_w;
			for (int j = i + 1; j < ribs; j++) {
				if (ListOfEdges[i][2] > ListOfEdges[j][2])
				{
					buff_f = ListOfEdges[i][0];
					buff_t = ListOfEdges[i][1];
					buff_w = ListOfEdges[i][2];
					ListOfEdges[i][0] = ListOfEdges[j][0];
					ListOfEdges[i][1] = ListOfEdges[j][1];
					ListOfEdges[i][2] = ListOfEdges[j][2];
					ListOfEdges[j][0] = buff_f;
					ListOfEdges[j][1] = buff_t;
					ListOfEdges[j][2] = buff_w;
				}
			}
		}
	}
public:
	Graph() {};
	Graph(int N) {
		type_represent = 'C';
		vertexs = N;
		ribs = 0;
		isWeighed=true;
		isOriented=false;
		AdjMatrix.resize(vertexs);
		for (int i = 0; i < vertexs; i++) {
			for (int j = 0; j < vertexs; j++) {
				AdjMatrix[i].push_back(0);
			}
		}
		
	};
	Graph(char type, bool oriented, bool weighed, int vertex, int rib)
	{
		vertexs=vertex;
		ribs=rib;
		type_represent=type;
		isWeighed=weighed;
	    isOriented=oriented;

		switch (type_represent)
		{
		case 'C':
		{
			AdjMatrix.resize(vertexs);
			for (int i = 0; i < vertexs; i++)
				AdjMatrix[i].resize(vertexs);
			break;
		}
		case 'L':
		{
			AdjList.resize(vertexs);
			break;
		}
		case 'E':
		{
			ListOfEdges.resize(ribs);
			for (int i = 0; i < ribs; i++)
				ListOfEdges[i].resize(3);
			break;
		}
		}
	}
	void readGraph(string fileName) {
		ifstream file(fileName);
		string buffer;
		//��������� ��� ������������� � �������������� ������
		file >> type_represent;
		switch (type_represent) {
		case 'C':case 'L': {
			file >> buffer;
			vertexs = stoi(buffer);
			break;
		}
		case 'E': {
			file >> buffer;
			vertexs = stoi(buffer);
			file >> buffer;
			ribs = stoi(buffer);
		}
		}
		//cout << type_represent << ' ' << vertexs << endl;
		//������ ������������ � ����������������� �����
		file >> buffer;
		isOriented = stoi(buffer);
		file >> buffer;
		isWeighed = stoi(buffer);
		//cout << isOriented << ' ' << isWeighed << endl;
		//������ ��� ���� � ����������� �� �������������

		switch (type_represent) {
			//������ ����, ���������� �������� ���������
		case 'C': {
			AdjMatrix.resize(vertexs);
			for (int i = 0; i < vertexs; i++) {
				for (int j = 0; j < vertexs; j++) {
					file >> buffer;
					AdjMatrix[i].push_back(stoi(buffer));
				}
			}
			break;
		}
				  //������ ����, ���������� ������� ������� ������
		case 'L': {
			AdjList.resize(vertexs);
			if (!isWeighed) {
				getline(file, buffer);
				for (int i = 0; i < vertexs; i++) {
					string num;
					getline(file, buffer);
					for (int j = 0; j <= buffer.length(); j++) {
						if ((buffer[j] != ' ') && (j != buffer.length())) {
							num += buffer[j];
						}
						else {
							pair<int, int> value_buff;
							value_buff.first = stoi(num);
							value_buff.second = 0;
							AdjList[i].push_back(value_buff);
							num = "";
						}
					}
				}
			}
			else {
				getline(file, buffer);
				for (int i = 0; i < vertexs; i++) {
					string num; pair<int, int> value_buff;
					getline(file, buffer);
					bool weigh_switch = false;
					for (int j = 0; j <= buffer.length(); j++) {
						if ((buffer[j] != ' ') && (j != buffer.length())) {
							num += buffer[j];
						}
						else {
							if (!weigh_switch) {
								value_buff.first = stoi(num);
								num = "";
								weigh_switch = !weigh_switch;
							}
							else {
								value_buff.second = stoi(num);
								num = "";
								weigh_switch = !weigh_switch;
								AdjList[i].push_back(value_buff);
							}
						}
					}
				}

			}
			break;
		}
				  //������ ����, ���������� ������� ������� ������
		case 'E': {
			ListOfEdges.resize(ribs);
			getline(file, buffer);
			for (int i = 0; i < ribs; i++) {
				string num;
				getline(file, buffer);
				for (int j = 0; j <= buffer.length(); j++) {
					if ((buffer[j] != ' ') && (j != buffer.length())) {
						num += buffer[j];
					}
					else {
						ListOfEdges[i].push_back(stoi(num));
						num = "";
					}
				}
			}


			break;
		}
		}
	};
	void addEdge(int from, int to, int weight) {
		switch (type_represent) {
		case 'C': {
			if (!isWeighed) {
				weight = 1;
			}
			if (isOriented) {
				AdjMatrix[from - 1][to - 1] = weight;
			}
			else {
				AdjMatrix[from - 1][to - 1] = weight;
				AdjMatrix[to - 1][from - 1] = weight;
			}
			break;
		}
		case 'L': {
			for (int i = 0; i < AdjList[from - 1].size() - 1; i++) {
				if (AdjList[from - 1][i].first == to) {
					return;
				}
			}
			if (!isWeighed) {
				weight = 0;
			}
			pair<int, int> buffer;
			if (isOriented) {
				buffer.first = to;
				buffer.second = weight;
				AdjList[from - 1].push_back(buffer);
			}
			else {
				buffer.first = to;
				buffer.second = weight;
				AdjList[from - 1].push_back(buffer);
				buffer.first = from;
				AdjList[to - 1].push_back(buffer);
			}
			break;
		}
		case 'E': {
			if (isOriented) {
				for (int i = 0; i < ribs; i++) {
					if ((ListOfEdges[i][0] == from) && (ListOfEdges[i][1] == to)) {
						return;
					}
				}
				ribs += 1;
				ListOfEdges.resize(ribs);
				ListOfEdges[ribs - 1].push_back(from);
				ListOfEdges[ribs - 1].push_back(to);
				if (isWeighed) {
					ListOfEdges[ribs - 1].push_back(weight);
				};
			}
			else {
				for (int i = 0; i < ribs; i++) {
					if ((ListOfEdges[i][0] == from) && (ListOfEdges[i][1] == to) || ((ListOfEdges[i][0] == to) && (ListOfEdges[i][1] == from))) {
						return;
					}
				}
				ribs += 1;
				ListOfEdges.resize(ribs);
				ListOfEdges[ribs - 1].push_back(from);
				ListOfEdges[ribs - 1].push_back(to);
				if (isWeighed) {
					ListOfEdges[ribs - 1].push_back(weight);
				};
			}
			break;
		}
		}
	};
	void removeEdge(int from, int to) {
		switch (type_represent) {
		case 'C': {
			AdjMatrix[from - 1][to - 1] = 0;
			if (!isOriented) {
				AdjMatrix[to - 1][from - 1] = 0;
			}
			break;
		}
		case 'L': {
			vector<pair<int, int>>::iterator iter = AdjList[from - 1].begin();
			while (iter != AdjList[from - 1].end()) {
				if (iter->first == to) {
					iter = AdjList[from - 1].erase(iter);

				}
				else {
					++iter;
				};
			}
			if (!isOriented) {
				vector<pair<int, int>>::iterator iter = AdjList[to - 1].begin();
				while (iter != AdjList[to - 1].end()) {
					if (iter->first == from) {
						iter = AdjList[to - 1].erase(iter);

					}
					else {
						++iter;
					}
				}
			}

			break;
		}
		case 'E': {
			for (int i = 0; i < ribs; i++) {
				if ((ListOfEdges[i][0] == from && ListOfEdges[i][1] == to) || (!isOriented && (ListOfEdges[i][0] == to && ListOfEdges[i][1] == from))) {
					ListOfEdges.erase(ListOfEdges.begin() + i);
					ribs--;
					//vector<vector<int>>(ListOfEdges).swap(ListOfEdges);
				}
			}
			break;
		}
		}

	};
	int changeEdge(int from, int to, int newWeight) {
		int weight_buff = 0;
		if (!isWeighed) { newWeight = sign(newWeight); };
		switch (type_represent) {
		case 'C': {
			weight_buff = AdjMatrix[from - 1][to - 1];
			AdjMatrix[from - 1][to - 1] = newWeight;
			if (!isOriented) {
				AdjMatrix[to - 1][from - 1] = newWeight;
			}
			return weight_buff;
			break;
		}
		case 'L': {
			for (int i = 0; i < AdjList[from - 1].size(); i++) {
				if (AdjList[from - 1][i].first == to) {
					weight_buff = AdjList[from - 1][i].second;
					AdjList[from - 1][i].second = newWeight;
					break;
				}
			}
			if (!isOriented) {
				for (int i = 0; i < AdjList[to - 1].size(); i++) {
					if (AdjList[to - 1][i].first == from) {
						AdjList[to - 1][i].second = newWeight;
						break;
					}
				}
				return weight_buff;
				break;
			}
		}
		case 'E': {
			for (int i = 0; i < ribs; i++) {
				if ((ListOfEdges[i][0] == from && ListOfEdges[i][1] == to) || (!isOriented && (ListOfEdges[i][0] == to && ListOfEdges[i][1] == from))) {
					weight_buff = ListOfEdges[i][2];
					ListOfEdges[i][2] = newWeight;
				}
			}
			return weight_buff;
			break;
		}
		}
	};
	void transformToAdjList() {
		AdjList.resize(vertexs);
		switch (type_represent) {
		case 'C': {
			for (int i = 0; i < vertexs; i++) {
				for (int j = 0; j < vertexs; j++) {
					if (AdjMatrix[i][j] != 0) {
						AdjList[i].push_back(make_pair(j + 1, AdjMatrix[i][j]));
					}
				}
			}
			AdjMatrix.clear();
			AdjMatrix.shrink_to_fit();
			break;
		}
		case 'L': {
			return;
			break;
		}
		case 'E': {
			for (int i = 0; i < ribs; i++) {
				if (isOriented) {
					if (isWeighed) {
						AdjList[ListOfEdges[i][0] - 1].push_back(make_pair(ListOfEdges[i][1], ListOfEdges[i][2]));
					}
					else {
						AdjList[ListOfEdges[i][0] - 1].push_back(make_pair(ListOfEdges[i][1], 1));
					}
				}
				else {//�����������������
					if (isWeighed) {
						AdjList[ListOfEdges[i][0] - 1].push_back(make_pair(ListOfEdges[i][1], ListOfEdges[i][2]));
						if (ListOfEdges[i][0] != ListOfEdges[i][1]) {
							AdjList[ListOfEdges[i][1] - 1].push_back(make_pair(ListOfEdges[i][0], ListOfEdges[i][2]));
						}
					}
					else {
						AdjList[ListOfEdges[i][0] - 1].push_back(make_pair(ListOfEdges[i][1], 1));
						if (ListOfEdges[i][0] != ListOfEdges[i][1]) {
							AdjList[ListOfEdges[i][1] - 1].push_back(make_pair(ListOfEdges[i][0], 1));
						}
					}
				}

			}
			ListOfEdges.clear();
			ListOfEdges.shrink_to_fit();
			break;
		}
		}
		type_represent = 'L';
	};
	void transformToAdjMatrix() {
		switch (type_represent) {
		case 'C': {
			return;
			break;
		}
		case 'L': {
			AdjMatrix.resize(vertexs);
			for (int i = 0; i < vertexs; i++) {
				AdjMatrix[i].resize(vertexs);
			}
			for (int i = 0; i < vertexs; i++) {
				for (int j = 0; j < AdjList[i].size(); j++) {
					if (AdjList[i][j].second != 0) {
						AdjMatrix[i][AdjList[i][j].first - 1] = AdjList[i][j].second;
						if (!isOriented) {
							AdjMatrix[AdjList[i][j].first - 1][i] = AdjList[i][j].second;
							this->removeEdge(AdjList[i][j].first, i + 1);
						}
					}
				}
			}
			AdjList.clear();
			AdjList.shrink_to_fit();
			break;
		}
		case 'E': {
			AdjMatrix.resize(vertexs);
			for (int i = 0; i < vertexs; i++) {
				AdjMatrix[i].resize(vertexs);

			}
			for (int j = 0; j < ribs; j++) {
				if (isWeighed) {
					AdjMatrix[ListOfEdges[j][0] - 1][ListOfEdges[j][1] - 1] = ListOfEdges[j][2];
					if (!isOriented) { AdjMatrix[ListOfEdges[j][1] - 1][ListOfEdges[j][0] - 1] = ListOfEdges[j][2]; }
				}
				else {
					AdjMatrix[ListOfEdges[j][0] - 1][ListOfEdges[j][1] - 1] = 1;
					if (!isOriented) { AdjMatrix[ListOfEdges[j][1] - 1][ListOfEdges[j][0] - 1] = 1; }
				}
			}
			ListOfEdges.clear();
			ListOfEdges.shrink_to_fit();
			break;
		}
		}
		type_represent = 'C';
	};
	void transformToListOfEdges() {
		switch (type_represent) {
		case 'C': {
			ribs = 0;
			for (int i = 0; i < vertexs; i++) {
				for (int j = 0; j < vertexs; j++) {
					if (AdjMatrix[i][j] > 0) {
						ribs++;
						ListOfEdges.resize(ribs);
						ListOfEdges[ListOfEdges.size() - 1].push_back(i + 1);
						ListOfEdges[ListOfEdges.size() - 1].push_back(j + 1);
						if (isWeighed) { ListOfEdges[ListOfEdges.size() - 1].push_back(AdjMatrix[i][j]); };
						if (!isOriented) {
							AdjMatrix[j][i] = 0;
						}
					}
				}
			}
			AdjMatrix.clear();
			AdjMatrix.shrink_to_fit();
			break;
		}
		case 'L': {
			ribs = 0;
			for (int i = 0; i < vertexs; i++) {
				for (int j = 0; j < AdjList[i].size(); j++) {
					ribs++;
					ListOfEdges.resize(ribs);
					ListOfEdges[ListOfEdges.size() - 1].push_back(i + 1);
					ListOfEdges[ListOfEdges.size() - 1].push_back(AdjList[i][j].first);
					if (isWeighed) { ListOfEdges[ListOfEdges.size() - 1].push_back(AdjList[i][j].second); }
					if (!isOriented) { this->removeEdge(AdjList[i][j].first, i + 1); }
				}
			}
			AdjList.clear();
			AdjList.shrink_to_fit();
			break;
		}
		case 'E': {
			return;
			break;
		}

		}
		type_represent = 'E';
	};
	void writeGraph(string fileName) {
		ofstream file(fileName);
		file << type_represent << ' ' << vertexs << ' ';
		if (type_represent == 'E') { file << ribs; }
		file << endl;
		file << isOriented << ' ' << isWeighed << endl;
		//����� � ���� � ����������� �� ���� �������������
		switch (type_represent) {
		case 'C': {
			for (int i = 0; i < vertexs; i++) {
				for (int j = 0; j < vertexs; j++) {
					file << AdjMatrix[i][j] << ' ';
				}
				file << endl;
			}
			file.close();
			break;
		}
		case 'L': {
			for (int i = 0; i < vertexs; i++) {
				for (int j = 0; j < AdjList[i].size(); j++) {
					file << AdjList[i][j].first;
					if (isWeighed) { file << ' ' << AdjList[i][j].second; }
					if (j != AdjList[i].size() - 1) { file << ' '; }
				}
				file << endl;
			}
			file.close();
			break;
		}
		case 'E': {
			for (int i = 0; i < ribs; i++) {
				file << ListOfEdges[i][0] << ' ' << ListOfEdges[i][1];
				if (isWeighed) { file << ' ' << ListOfEdges[i][2]; }
				file << endl;
			}
			file.close();
			break;
		}
		}

	};

	//������������ 2
	Graph getSpaingTreePrima() {
		Graph graph(vertexs);
		char before = type_represent;
		transformToAdjMatrix();
		int n = 0;
		set<int> used;
		used.insert(0);
		while (n < vertexs - 1) {
			int from, to;
			int weight = 100000;
			for (int i = 0; i < vertexs; i++)
			{
				if (used.count(i) != 0) {
					for (int j = 0; j < vertexs; j++)
					{
						if ((weight > AdjMatrix[i][j])&&(used.count(j)==0)&&(AdjMatrix[i][j]!=0)) {
							weight = AdjMatrix[i][j];
							from = i;
							to = j;
						}
					}
				}
			}
			graph.AdjMatrix[from][to] = weight;
			graph.AdjMatrix[to][from] = weight;
			used.insert(to);
			n++;

		}
		switch (before) {
		case 'C': {
			graph.transformToAdjMatrix();
			break;
		}
		case 'L':{
			graph.transformToAdjList();
			break;
		}
		case 'E': {
			graph.transformToListOfEdges();
			break;
		}
		}
		return graph;
	};
	Graph getSpaingTreeKruscal() {
		Graph graph(vertexs);
		char before = type_represent;
		ListSort(*this);
		DSU dsu(vertexs);

		for (int i = 0; i < ribs; i++)
		{
			int from = ListOfEdges[i][0];
			int to = ListOfEdges[i][1];
			int weight = ListOfEdges[i][2];
			if (dsu.find(from) != dsu.find(to))
			{
				graph.addEdge(from, to, weight);
				dsu.unite(from, to);
			}
		}
		switch (before) {
		case 'C': {
			graph.transformToAdjMatrix();
			break;
		}
		case 'L': {
			graph.transformToAdjList();
			break;
		}
		case 'E': {
			graph.transformToListOfEdges();
			break;
		}
		}
		return graph;
	};
	Graph getSpaingTreeBoruvka() {
		
		DSU dsu(vertexs);
		Graph graph(vertexs);
		char before = type_represent;
		transformToListOfEdges();
		int vert_buffer = vertexs;
		while (vert_buffer > 1)
		{
			bool isChanged = false;
			vector<int> key(vertexs + 1, -1);

			for (int i = 0; i < ribs; i++)
			{
				int from_dsu = dsu.find(ListOfEdges[i][0]);
				int to_dsu = dsu.find(ListOfEdges[i][1]);
				int weight = ListOfEdges[i][2];

				if (from_dsu != to_dsu)
				{
					if (key[from_dsu] == -1 || weight < ListOfEdges[key[from_dsu]][2])
					{
						key[from_dsu] = i;
						isChanged = true;
					}
					if (key[to_dsu] == -1 || weight < ListOfEdges[key[to_dsu]][2])
					{
						key[to_dsu] = i;
						isChanged = true;
					}
				}
			}

			if (!isChanged)
				break;

			for (int i = 1; i <= vertexs; i++)
			{
				if (key[i] != -1)
				{
					int from = ListOfEdges[key[i]][0];
					int to = ListOfEdges[key[i]][1];
					int weight = ListOfEdges[key[i]][2];
					if (dsu.find(from) != dsu.find(to))
					{
						graph.addEdge(from, to, weight);
						dsu.unite(from, to);
						vert_buffer--;
					}
				}
			}
		}
		switch (before) {
		case 'C': {
			graph.transformToAdjMatrix();
			break;
		}
		case 'L': {
			graph.transformToAdjList();
			break;
		}
		case 'E': {
			graph.transformToListOfEdges();
			break;
		}
		}
		return graph;
	};
	//������������ 3
	bool checkBridge(int from_v, int to_v)
	{
		Graph graph('L', isOriented, isWeighed, vertexs, ribs);
		DSU dsu(vertexs);
		char before = type_represent;
		transformToAdjMatrix();
		graph.transformToAdjMatrix();
		for (int i = 0; i < vertexs; i++)
		{
			for (int j = 0; j < vertexs; j++)
			{
				graph.AdjMatrix[i][j] = AdjMatrix[i][j];
			}
			
		}
		graph.removeEdge(from_v, to_v);

		for (int i = 0; i < vertexs; i++)
		{
			int from = i + 1;
			for (int j = 0; j < vertexs; j++)
			{
				int to = j+1;
				if ((dsu.find(from) != dsu.find(to))&&(graph.AdjMatrix[i][j]!=0))
				{
					dsu.unite(from, to);
				}
			}
		}
		switch (before) {
		case 'C': {
			transformToAdjMatrix();
			break;
		}
		case 'L': {
			transformToAdjList();
			break;
		}
		case 'E': {
			transformToListOfEdges();
			break;
		}		  
		}
		return !dsu.isSingleTree();
	}
	int checkEuler(bool &circleExist)
	{
		DSU dsu(vertexs);
		int odd_count = 0;
		int first_vertex = 1;
		vector<int> power(vertexs + 1, 0);
		char before = type_represent;
		transformToListOfEdges();

		for (int i = 0; i < ribs; i++)
		{
			power[ListOfEdges[i][0]]++;
			power[ListOfEdges[i][1]]++;
		}

		for (int i = 1; i <= vertexs; i++)
		{
			if (power[i] % 2 == 1)
			{
				odd_count++;
				first_vertex = i;
			}
		}

		for (int i = 0; i < ribs; i++)
		{
			int from = ListOfEdges[i][0];
			int to = ListOfEdges[i][1];
			int weight = ListOfEdges[i][2];
			if (dsu.find(from) != dsu.find(to))
			{
				dsu.unite(from, to);
			}
		}
		switch (before) {
		case 'C': {
			transformToAdjMatrix();
			break;
		}
		case 'L': {
			transformToAdjList();
			break;
		}
		case 'E': {
			transformToListOfEdges();
			break;
		}
		}
		bool isSingleTree = false;
		isSingleTree = dsu.isSingleTree();
		if ((odd_count == 0 || odd_count == 2) && isSingleTree)
		{
			circleExist = true;
			return first_vertex;
		}
		else
		{
			circleExist = false;
			return 0;
		}
	}
	vector<int> getEuleranTourFleri()
	{
		Graph graph('E', isOriented, isWeighed, vertexs, ribs);
		vector<int> tour(ribs + 1);
		vector<int> power(vertexs + 1, 0);
		transformToListOfEdges();
		for (int i = 0; i < ribs; i++)
		{
			power[ListOfEdges[i][0]]++;
			power[ListOfEdges[i][1]]++;
			graph.ListOfEdges[i][0] = ListOfEdges[i][0];
			graph.ListOfEdges[i][1] = ListOfEdges[i][1];
			graph.ListOfEdges[i][2] = ListOfEdges[i][2];
		}

		bool circleExist;
		int current = checkEuler(circleExist);
		tour[0] = current;
		transformToAdjList();
		graph.transformToAdjList();

		for (int i = 0; i < ribs; i++)
		{
			int ribs_count = power[current];
			bool isChanged = false;
			for (int j = 0; j < ribs_count; j++)
			{
				int second = graph.AdjList[current-1][j].first;
				if (!graph.checkBridge(current, second) || power[current] == 1)
				{
					isChanged = true;
					power[current]--;
					graph.removeEdge(current, second);
					current = second;
					tour[i + 1] = current;
					power[second]--;
					break;
				}
			}

			if (!isChanged)
				for (int j = 0; j < ribs_count; j++)
				{
					int second = graph.AdjList[current-1][j].first;
					if (!graph.checkBridge(current, second) || power[current] != 1)
					{
						power[current]--;
						graph.removeEdge(current, second);
						current = second;
						tour[i + 1] = current;
						power[second]--;
						break;
					}
				}
			isChanged = false;
		}
		return tour;
	}
	vector<int> getEuleranTourEffective() {
		vector<int> tour;
		bool circleExist;
		int start = checkEuler(circleExist);
		if (start == 0)
		{
			return tour;
		}

		transformToAdjList();
		Graph graph(*this);
		stack<int> buffer;
		buffer.push(start);

		while (!buffer.empty())
		{
			int current = buffer.top();
			int count = graph.AdjList[current-1].size();
			for (int i=0;i<count;i++)
			{
				int next = graph.AdjList[current - 1][i].first;
				buffer.push(next);
				graph.removeEdge(current, next);
				break;
			}
			if (current == buffer.top())
			{
				buffer.pop();
				tour.push_back(current);
			}
		}
		return tour;
	}
	//������������ 4
	int checkBipart(std::vector<char> &marks)
	{
		queue<int> buffer;
		Graph graph(*this);
		graph.transformToAdjMatrix();
		marks.resize(vertexs);
		for (int i = 0; i < vertexs; i++)
		{
			if (marks[i] != 'A' && marks[i] != 'B')
			{
				marks[i] = 'A';
				buffer.push(i);
				int to;
				while (!buffer.empty())
				{
					to = buffer.front();
					buffer.pop();
					for (int from = 0; from < vertexs; from++)
					{
						if (graph.AdjMatrix[to][from] != 0)
						{
							if (marks[from] != 'A' && marks[from] != 'B')
							{
								if (marks[to] == 'B')
									marks[from] = 'A';
								else
									marks[from] = 'B';
								buffer.push(from);
							}
							if (marks[from] == marks[to])
								return 0;
						}
					}
				}
			}
		}
		return 1;
	}
	bool khun(vector<int> &right_array, vector<int> &left_array, vector<bool> &mark, vector<int> &right_conn, vector<int> &left_conn, int u, Graph &graph)
	{
		for (int i = 0; i < right_array.size(); i++)
		{
			if (graph.AdjMatrix[right_array[i]-1][left_array[u]-1] != 0 && !mark[i])
			{
				mark[i] = true;
				if (right_conn[i] == -1 || khun(right_array, left_array, mark, right_conn, left_conn, right_conn[i],graph))
				{
					left_conn[u] = i;
					right_conn[i] = u;
					return true;
				}
			}
		}
		return false;
	}
	vector<pair<int, int>> getMaximumMatchingBipart()
	{
		Graph graph(*this);
		vector<char> marks(vertexs + 1);
		vector<int> left_array(0), right_array(0);
		checkBipart(marks);
		for (int i = 0; i < vertexs; i++)
		{
			if (marks[i] == 'A')
				left_array.push_back(i+1);
			if (marks[i] == 'B')
				right_array.push_back(i+1);
		}

		if (left_array.size() > right_array.size())
			swap(left_array, right_array);
		vector<int> right_conn(right_array.size(), -1), left_conn(left_array.size(), -1);
		graph.transformToAdjMatrix();
		for (int i = 0; i < left_conn.size(); i++)
		{
			for (int j = 0; j < right_conn.size(); j++)
			{
				if (left_conn[i] == -1 && right_conn[j] == -1 && graph.AdjMatrix[left_array[i]-1][right_array[j]-1] != 0)
				{
					left_conn[i] = j;
					right_conn[j] = i;
				}
			}
		}
		vector<bool> mark(right_conn.size(), false);
		for (int i = 0; i < left_conn.size(); i++)
		{
			if (left_conn[i] == -1)
			{
				mark.assign(right_conn.size(), false);
				khun(right_array, left_array, mark, right_conn, left_conn, i, graph);
			}
		}

		vector<pair<int, int> > result;
		pair<int, int> buffer(0, 0);
		for (int i = 0; i < left_conn.size(); i++)
		{
			if (left_conn[i] != -1)
			{
				buffer.first = left_array[i];
				buffer.second = right_array[left_conn[i]];
				result.push_back(buffer);
			}
		}
		return result;
	}
};